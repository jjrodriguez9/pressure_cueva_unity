function J = Mpc(cv,sp,p,a2,k,Ni,Nf,Nu,ts,alpha,beta)

%% PARAMETROS DEL TANQUE
    k1=0.08684219; 
    k2=0.07908202;
    C =0.53821693;
    R =1.39999718;
    
%% PRESIONES DEL TANQUE
pi=100; %%(PSI)  %% PRESION DE ENTRADA (ALIMENTACION DEL COMPRESOR)
po=0;   %%(PSI)  %% PRESION DE SALIDA (COMO SALE AL AMBIENTE NO TIENE PRESION DE SALIDA)

Jhe=0;
Jdu=0;


for j=Ni:Nf

 
pp =C*(cv(1)/100*k1*sqrt(pi*(pi-p))-(a2*k2*sqrt(p*(p-po)))); 
p=pp*ts+p;

he=(sp(j+k)-p)^2;

Jhe=Jhe+he;
end

for j=1:Nu
if j==1
    a1_a=0;
else
    a1_a=cvref;
end
 cvref=cv(j);
 
 du=(cvref-a1_a)^2;

Jdu=Jdu+du;
end

J=alpha*Jhe+beta*Jdu;
end