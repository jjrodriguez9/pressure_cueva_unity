function Unity=Recibir_Unity

d1 = calllib('smClient64','getFloat','UNITY',0);
d2 = calllib('smClient64','getFloat','UNITY',1);
d3 = calllib('smClient64','getFloat','UNITY',2);
d4 = calllib('smClient64','getFloat','UNITY',3);
d5 = calllib('smClient64','getFloat','UNITY',4);
d6 = calllib('smClient64','getFloat','UNITY',5);
d7 = calllib('smClient64','getFloat','UNITY',6);

Unity=[d1 d2 d3 d4 d5 d6 d7]';
end