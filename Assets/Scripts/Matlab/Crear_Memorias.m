function Crear_Memorias
loadlibrary('smClient64.dll','./smClient.h');

calllib('smClient64','createMemory','UNITY',6,2);   
calllib('smClient64','createMemory','MATLAB',3,2);
calllib('smClient64','createMemory','ENABLE',2,1);  

calllib('smClient64','openMemory','UNITY',2);
calllib('smClient64','openMemory','MATLAB',2);
calllib('smClient64','openMemory','ENABLE',1);

for k=1:6
calllib('smClient64','setFloat','UNITY',k,0); 
calllib('smClient64','setFloat','MATLAB',k,0);
end

end