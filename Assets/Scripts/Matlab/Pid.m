function a1=Pid(Kp,Ti,Td,ts,umax,umin,e,k,a1a)


q0=Kp*(1+ts/(2*Ti)+Td/ts);
q1=-Kp*(1-ts/(2*Ti)+(2*Td)/ts);
q2=(Kp*Td)/ts;



if(k>=3)
    cv=  q0*e(k) + q1*e(k-1) + q2*e(k-2)+a1a(k-1);
    a1=min(umax,max(umin,cv));
    
else

    a1=0;
    
end
end