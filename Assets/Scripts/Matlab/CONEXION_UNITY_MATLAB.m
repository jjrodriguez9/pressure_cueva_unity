%% LIMPIEZA 
clear all; 
close all; 
warning off; 
clc;

%% TIEMPO DE MUESTREO
ts=0.1; %%[Segundos]

%% CREACION DE MEMORIAS
Crear_Memorias();

k=1;
MATLAB=0;
flag=0;
%% HABILITACION
disp('ESPERANDO QUE INICE UNITY..........')


while(MATLAB==0)
MATLAB=calllib('smClient64','getInt','ENABLE',0);
end

disp('LA COMUNICACION INCIO ENTRE UNITY Y MATLAB');

%% LIMITES MAXIMOS Y MINIMOS DE PID
cvmax = 100.0; %%(100 %) 
cvmin = 0.0; %%(0%)

%% OPCIONES DEL MPC
options = optimset('Algorithm','sqp','Display','off'); 

%% DECLARACION DE VECTOR
spp=[];

%% INICIO DE HORIZONTE DE PREDICCION

cv(1)=0;sp(1)=0;pv(1)=0;load(1)=0;e(1)=0;dt(1)=0;cpu(1)=0;

Fuzzy=readfis('simuderivaada.fis');

while (MATLAB==1)  
    
    Start=tic;
    
    %% SELECCION
    SELECTOR=calllib('smClient64','getInt','ENABLE',1);
    switch SELECTOR
    
    %% OPEN LOOP   
    case 1
    if (flag ~=1)
        Guardar(flag,sp,pv,cv,e,load,ts,k,dt,cpu);
        disp("OPEN LOOP");
        cv=[];pv=[];load=[];dt=[];cpu=[];
        k=1;flag=1;
    end
    %% Recepcion de Datos Unity
    Unity=Recibir_Unity();
    pv(k)=Unity(1);cv(k)=Unity(2);load(k)=Unity(3);
   
    %% CONTROL PID
    case 2
    if (flag ~=2)
        Guardar(flag,sp,pv,cv,e,load,ts,k,dt,cpu);
        disp("CONTROL PID");
        cv=[];pv=[];sp=[];load=[];e=[];cpu=[];dt=[];
        k=1;flag=2; 
    end
    %% Recepcion de Datos Unity
    Unity=Recibir_Unity();
    pv(k)=Unity(1);sp(k)=Unity(2);load(k)=Unity(3);
    Kp=Unity(4);Ti=Unity(5);Td=Unity(6);
    %% CALCULO DEL ERROR
    e(k)=sp(k)-pv(k);
    %% CALCULA DE LA ACCION DE CONTROL PID
    TC=cputime;
    cv(k)=Pid(Kp,Ti,Td,ts,cvmax,cvmin,e,k,cv);
    cpu(k)=cputime-TC;
    %% ENVIAR UNITY
    Matlab=[cv(k) 0 0]';
    Enviar_Unity(Matlab);
    
    %% CONTROL MPC
    case 3
    if (flag ~=3)
        Guardar(flag,sp,pv,cv,e,load,ts,k,dt,cpu);
        disp("CONTROL MPC");
        cv=[];pv=[];sp=[];load=[];e=[];cpu=[];dt=[];
        spp=[];vi=[];
        k=1;flag=3; 
    end 
    %% RECIBIR DATOS DE UNITY
    Unity=Recibir_Unity();
    pv(k)=Unity(1);sp(k)=Unity(2);load(k)=Unity(3);
    Na=Unity(4);Nu=Unity(5);alpha=Unity(6);beta=Unity(7);
    %% VECTOR DE SP
    if k==1
    spp=[spp,sp(k)*ones(1,Na+k)];
    vi=repmat(0,Nu,1);
    else
    spp=[spp,sp(k)*ones(1,1)];
    end
    Ni=1;
    %% CALCULO DEL ERROR
    e(k)=sp(k)-pv(k); 
    %% CALCULA LA ACCION DE CONTROL MPC
    TC=cputime;
    Ompc  = @(vi) Mpc(vi,spp,pv(k),load(k),k,Ni,Na,Nu,ts,alpha,beta);
    vi    = fmincon(Ompc,vi,[],[],[],[],cvmin,cvmax,[],options);
    cv(k)=vi(1);
    cpu(k)=cputime-TC;
    %% ENVIAR UNITY
    Matlab=[cv(k) 0 0]';
    Enviar_Unity(Matlab);
    
   
    %% CONTROL FUZZY
    case 4
    if (flag ~=4)
        Guardar(flag,sp,pv,cv,e,load,ts,k,dt,cpu);
        disp("CONTROL FUZZY");
        cv=[];pv=[];sp=[];load=[];e=[];cpu=[];dt=[];
        u=[];du=[];de=[];
        k=1;flag=4; 
    end
    Unity=Recibir_Unity();
    pv(k)=Unity(1);sp(k)=Unity(2);load(k)=Unity(3);
    %% CALCULO DEL ERROR
    e(k)=sp(k)-pv(k);

    % disp(he(k))
    if(k==1)
    de(k)=e(k)/ts;
    else
    de(k)=(e(k)-e(k-1))/ts;
    end
    %% FUZZY
    TC=cputime;
    du(k)=(evalfis([tanh(e(k)) tanh(de(k))],Fuzzy));
   
    if k==1
    u(k)=du(k);
    else
    u(k)=u(k-1)+du(k);
    end
    
    cv(k)=min(cvmax,max(cvmin,u(k)));
    cpu(k)=cputime-TC;
    
    %% ENVIAR UNITY
    Matlab=[cv(k) 0 0]';
    Enviar_Unity(Matlab);
    
    end
    %% ASEGURACION TIEMPO MUESTREO
    while(toc(Start)<ts)     
    end
    
    %% ALMACENAMIENTO DEL TIEMPO DE MUESTREO
    dt(k)=toc(Start);
    
    %% GENRACION DEL SIGUIENTE VALOR
    k=k+1; 
    MATLAB=calllib('smClient64','getInt','ENABLE',0);
end

disp('END');
Guardar(flag,sp,pv,cv,e,load,ts,k,dt,cpu);
pause(2);
calllib('smClient64','freeMemories');



function Guardar(dato,sp,pv,cv,e,load,ts,k,dt,cpu)

if(dato==1)save('OpenLoop.mat','cv','pv','load','ts','k','dt','cpu');end
if(dato==2)save('PID.mat','cv','pv','load','sp','e','ts','k','dt','cpu');end
if(dato==3)save('MPC.mat','cv','pv','load','sp','e','ts','k','dt','cpu');end
if(dato==4)save('FUZZY.mat','cv','pv','load','sp','e','ts','k','dt','cpu');end
end

