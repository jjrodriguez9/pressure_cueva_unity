clc
clear all
close all
load('OpenLoop.mat');
figure(1)

tf=(k*ts)-ts;
t=0:ts:tf;
plot(t(1:length(pv)),pv,'b','LineWidth',2);hold on;grid on
plot(t(1:length(cv)),cv*100,'r','LineWidth',2);legend('PV','CV')
xlim([0 tf])
title('OPEN LOOP');

pause(1)
clc
clear all
load('PID.mat');
figure(2)
tf=(k*ts)-ts;
t=0:ts:tf;
subplot(5,1,1)
plot(t(1:length(pv)),pv,'g','LineWidth',2);hold on;grid on
plot(t(1:length(sp)),sp,'b','LineWidth',2);legend('PV','SP');xlim([0 tf])
subplot(5,1,2)
plot(t(1:length(e)),e,'r','LineWidth',2);grid on;legend('ERROR');xlim([0 tf])
subplot(5,1,3)
plot(t(1:length(cv)),cv,'m','LineWidth',2);hold on;grid on
plot(t(1:length(load)),load*100,'--c','LineWidth',2);legend('CV','Load');xlim([0 tf])
subplot(5,1,4)
plot(t(1:length(dt)),dt,'k','LineWidth',2);grid on;legend('Tiempo de Muestreo');xlim([0 tf])
subplot(5,1,5)
plot(t(1:length(cpu)),cpu,'c','LineWidth',2);grid on;legend('Tiempo de Ejecucion Pid')
xlim([0 tf])


pause(1)
clc
clear all
load('MPC.mat');
figure(3)
tf=(k*ts)-ts;
t=0:ts:tf;
subplot(5,1,1)
plot(t(1:length(pv)),pv,'g','LineWidth',2);hold on;grid on
plot(t(1:length(sp)),sp,'b','LineWidth',2);legend('PV','SP');xlim([0 tf])
subplot(5,1,2)
plot(t(1:length(e)),e,'r','LineWidth',2);grid on;legend('ERROR');xlim([0 tf])
subplot(5,1,3)
plot(t(1:length(cv)),cv,'m','LineWidth',2);hold on;grid on
plot(t(1:length(load)),load*100,'--c','LineWidth',2);legend('CV','Load');xlim([0 tf])
subplot(5,1,4)
plot(t(1:length(dt)),dt,'k','LineWidth',2);grid on;legend('Tiempo de Muestreo');xlim([0 tf])
subplot(5,1,5)
plot(t(1:length(cpu)),cpu,'c','LineWidth',2);grid on;legend('Tiempo de Ejecucion MPC')
xlim([0 tf])

pause(1)
clc
clear all
load('FUZZY.mat');
figure(4)
tf=(k*ts)-ts;
t=0:ts:tf;
subplot(5,1,1)
plot(t(1:length(pv)),pv,'g','LineWidth',2);hold on;grid on
plot(t(1:length(sp)),sp,'b','LineWidth',2);legend('PV','SP');xlim([0 tf])
subplot(5,1,2)
plot(t(1:length(e)),e,'r','LineWidth',2);grid on;legend('ERROR');xlim([0 tf])
subplot(5,1,3)
plot(t(1:length(cv)),cv,'m','LineWidth',2);hold on;grid on
plot(t(1:length(load)),load*100,'--c','LineWidth',2);legend('CV','Load');xlim([0 tf])
subplot(5,1,4)
plot(t(1:length(dt)),dt,'k','LineWidth',2);grid on;legend('Tiempo de Muestreo');xlim([0 tf])
subplot(5,1,5)
plot(t(1:length(cpu)),cpu,'c','LineWidth',2);grid on;legend('Tiempo de Ejecucion FUZZY')
xlim([0 tf])

