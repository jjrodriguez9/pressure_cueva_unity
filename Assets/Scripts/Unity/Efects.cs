
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Efects : MonoBehaviour
{
    [SerializeField] AudioClip motor;
    AudioSource audioSource;

    [SerializeField] ParticleSystem gas;
    [SerializeField] ParticleSystem salida;
    [SerializeField] ParticleSystem drenaje;

    [SerializeField] GameObject tanque;
    AudioSource atanque;
    [SerializeField] AudioClip stanque;

    private void Awake()

    {
        audioSource = GetComponent<AudioSource>();
        atanque = tanque.GetComponent<AudioSource>();


        var emission = gas.emission;
        emission.enabled = false;

        var emi = salida.emission;
        emi.enabled = false;

        var dre = drenaje.emission;
        dre.enabled = false;
    }


    private void Update()
    {
        var emission = gas.emission;
        var emi = salida.emission;
        var dre = drenaje.emission;


        if (Setting.setting._pv <= 9f) { this.Efecto(true); } else { this.Efecto(false); }

        if (Setting.setting._pv > 0) { emission.enabled = true; this.Tanque(true); } else { emission.enabled = false; this.Tanque(false); }

        if (Setting.setting._di > 0) { emi.enabled = true; } else { emi.enabled = false; }

        if (Setting.setting._dmaximo) { dre.enabled = true; } else { dre.enabled = false; }

    }


    public void Efecto(bool activador)
    {
        if (activador) { audioSource.PlayOneShot(motor); }

    }

    public void Tanque(bool activador)
    {
        if (activador) { audioSource.PlayOneShot(stanque); }

    }

}
