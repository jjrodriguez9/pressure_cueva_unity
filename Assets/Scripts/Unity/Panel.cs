﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Panel : MonoBehaviour
{
    [SerializeField] private TMP_Text[] MANUALT;
    [SerializeField] private TMP_Text[] PIDT;
    [SerializeField] private TMP_Text[] MPCT;
    [SerializeField] private TMP_Text[] FUZZYT;
    [SerializeField] private TMP_Text[] CVT;
    [SerializeField] private TMP_Text[] LOADT;
    [SerializeField] private TMP_Text Transmisor;
    [SerializeField] private GameObject[] VENTANAS;
    [SerializeField] Button[] Luz;


    void Start()
    {
        var gray = Luz[0].GetComponent<Button>().colors; gray.normalColor = Color.gray;

        for (int i = 0; i < MANUALT.Length; i++) { MANUALT[i].text = System.Convert.ToString(0); }

        for (int i = 0; i < PIDT.Length; i++) { PIDT[i].text = System.Convert.ToString(0); }

        for (int i = 0; i < MPCT.Length; i++) { MPCT[i].text = System.Convert.ToString(0); }

        for (int i = 0; i < FUZZYT.Length; i++) { FUZZYT[i].text = System.Convert.ToString(0); }

        for (int i = 0; i < CVT.Length; i++) { MANUALT[i].text = System.Convert.ToString(0); }

        for (int i = 0; i < LOADT.Length; i++) { MANUALT[i].text = System.Convert.ToString(0); }

        for (int i = 0; i < VENTANAS.Length; i++) { VENTANAS[i].SetActive(false); }

        for (int i = 0; i < Luz.Length; i++) { Luz[i].GetComponent<Button>().colors = gray; }

        Transmisor.text = System.Convert.ToString(0);

        // ACTIVO LA VENTANA MENU
        VENTANAS[0].SetActive(true);

    }


    void Update()
    {

        if (Setting.setting._selector.Equals(1)) { this.MANUAL(); }
        if (Setting.setting._selector.Equals(2)) { this.PID(); }
        if (Setting.setting._selector.Equals(3)) { this.MPC(); }
        if (Setting.setting._selector.Equals(4)) { this.FUZZY(); }

        Transmisor.text = System.Convert.ToString(System.Math.Round(Setting.setting._pv, 3));

        if (Input.GetKey(KeyCode.Q)) { Setting.setting._camara = true; }
        if (Input.GetKey(KeyCode.E)) { Setting.setting._camara = false; }

        this.ActivadorVentanas(Setting.setting._selector);

        this.ValvulaIngreso();
        this.ValvulaSalida();

    }


    private void MANUAL()
    {
        MANUALT[0].text = System.Convert.ToString(System.Math.Round(Setting.setting._cv * 100f, 3));
        MANUALT[1].text = System.Convert.ToString(System.Math.Round(Setting.setting._di * 100f, 3));
        MANUALT[2].text = System.Convert.ToString(System.Math.Round(Setting.setting._pv, 2));

    }
    private void PID()
    {
        PIDT[0].text = System.Convert.ToString(System.Math.Round(Setting.setting._sp, 2));
        PIDT[1].text = System.Convert.ToString(System.Math.Round(Setting.setting._pv, 3));
        PIDT[2].text = System.Convert.ToString(System.Math.Round(Setting.setting._cv * 100f, 3));
        PIDT[3].text = System.Convert.ToString(System.Math.Round(Setting.setting._di * 100f, 3));
        PIDT[4].text = System.Convert.ToString(System.Math.Round(Setting.setting._he, 3));
        PIDT[5].text = System.Convert.ToString(System.Math.Round(Setting.setting._kp, 4));
        PIDT[6].text = System.Convert.ToString(System.Math.Round(Setting.setting._Ti, 4));
        PIDT[7].text = System.Convert.ToString(System.Math.Round(Setting.setting._Td, 4));

    }

    private void MPC()
    {
        MPCT[0].text = System.Convert.ToString(System.Math.Round(Setting.setting._sp, 2));
        MPCT[1].text = System.Convert.ToString(System.Math.Round(Setting.setting._pv, 3));
        MPCT[2].text = System.Convert.ToString(System.Math.Round(Setting.setting._cv * 100f, 3));
        MPCT[3].text = System.Convert.ToString(System.Math.Round(Setting.setting._di * 100f, 3));
        MPCT[4].text = System.Convert.ToString(System.Math.Round(Setting.setting._he, 3));
        MPCT[5].text = System.Convert.ToString(System.Math.Round(Setting.setting._Na, 4));
        MPCT[6].text = System.Convert.ToString(System.Math.Round(Setting.setting._Nu, 4));
        MPCT[7].text = System.Convert.ToString(System.Math.Round(Setting.setting._alpha, 4));
        MPCT[8].text = System.Convert.ToString(System.Math.Round(Setting.setting._beta, 4));
    }


    private void FUZZY()
    {
        FUZZYT[0].text = System.Convert.ToString(System.Math.Round(Setting.setting._sp, 2));
        FUZZYT[1].text = System.Convert.ToString(System.Math.Round(Setting.setting._pv, 3));
        FUZZYT[2].text = System.Convert.ToString(System.Math.Round(Setting.setting._cv * 40f, 3));
        FUZZYT[3].text = System.Convert.ToString(System.Math.Round(Setting.setting._di * 100f, 3));
        FUZZYT[4].text = System.Convert.ToString(System.Math.Round(Setting.setting._he, 3));


    }

    private void ValvulaIngreso()
    {
        CVT[0].text = System.Convert.ToString(System.Math.Round(Setting.setting._cv * 100f, 2));
        CVT[1].text = System.Convert.ToString(System.Math.Round(Setting.setting._cv * 0.1 * 100f + 5.0f, 3));
        CVT[2].text = System.Convert.ToString(System.Math.Round(Setting.setting._cv * 0.16f * 100f + 4.0f, 3));
    }

    private void ValvulaSalida()
    {
        LOADT[0].text = System.Convert.ToString(System.Math.Round(Setting.setting._di * 100f, 3));
        //     LOADT[1].text = System.Convert.ToString(System.Math.Round(Setting.setting._pv, 3));
        //     LOADT[2].text = System.Convert.ToString(System.Math.Round(Setting.setting._cv * 100f, 3));
    }

    private void ActivadorVentanas(int v)
    {
        var gray = Luz[0].GetComponent<Button>().colors; gray.normalColor = Color.gray;
        var green = Luz[0].GetComponent<Button>().colors; green.normalColor = Color.green;

        if (v >= 1)
        {
            for (int i = 0; i < VENTANAS.Length; i++) { VENTANAS[i].SetActive(false); }
            for (int i = 0; i < Luz.Length; i++) { Luz[i].GetComponent<Button>().colors = gray; }

            VENTANAS[v].SetActive(true);
            Luz[v - 1].GetComponent<Button>().colors = green;
            VENTANAS[5].SetActive(true);
        }

    }
}
