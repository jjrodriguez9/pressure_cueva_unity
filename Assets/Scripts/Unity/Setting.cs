﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Setting : MonoBehaviour
{
    public static Setting setting;

    [SerializeField]
    private float pi;
    public float _pi { set { this.pi = value; } get { return pi; } }

    [SerializeField]
    private float po;
    public float _po { set { this.po = value; } get { return po; } }


    [SerializeField]
    private float pv;
    public float _pv { set { this.pv = value; } get { return pv; } }

    private float cv, di, he, sp = 10f;
    private float kp = 8.18125f, Ti = 17.256f, Td = 0.0001170f, v2a;

    private float Na = 45f, Nu = 3f, alpha = 10f, beta = 0.1f;

    private int selector;
    private bool camara, dmaximo;

    public int _selector { set { this.selector = value; } get { return selector; } }
    public bool _camara { set { this.camara = value; } get { return camara; } }

    public bool _dmaximo { set { this.dmaximo = value; } get { return dmaximo; } }
    // Constantes de Control PID
    public float _kp { set { this.kp = value; } get { return kp; } }
    public float _Ti { set { this.Ti = value; } get { return Ti; } }
    public float _Td { set { this.Td = value; } get { return Td; } }

    // Constantes de Control MPC\
    public float _Na { set { this.Na = value; } get { return Na; } }
    public float _Nu { set { this.Nu = value; } get { return Nu; } }
    public float _alpha { set { this.alpha = value; } get { return alpha; } }
    public float _beta { set { this.beta = value; } get { return beta; } }

    // Variables de Control del Proceso
    public float _cv { set { this.cv = value; } get { return cv; } }
    public float _di { set { this.di = value; } get { return di; } }
    public float _he { set { this.he = value; } get { return he; } }
    public float _sp { set { this.sp = value; } get { return sp; } }

    public float _v2a { set { this.v2a = value; } get { return v2a; } }

    public string control = " ";
    public string _control { set { this.control = value; } get { return control; } }

    private void Awake()
    {
        if (Setting.setting == null)
        {
            Setting.setting = this;
        }
        else
        {
            if (Setting.setting != this)
            {
                Destroy(this.gameObject);
            }
        }
        DontDestroyOnLoad(this.gameObject);
    }
}


