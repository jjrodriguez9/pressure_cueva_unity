﻿using UnityEngine;
using ChartAndGraph;

public class Graficas : MonoBehaviour
{
    public GraphChart[] G;
    float lastTime = 0f;
    private float ts = 0.1f;

    void Start()
    {
        G[0].AutoScrollHorizontally = true;
        //G[0].DataSource.HorizontalViewSize = 10f;
        G[0].DataSource.StartBatch();
        G[0].DataSource.ClearCategory("pv");
        G[0].DataSource.ClearCategory("sp");
        G[0].DataSource.EndBatch();

        G[1].AutoScrollHorizontally = true;
        // G[0].DataSource.HorizontalViewSize = 10f;
        G[1].DataSource.StartBatch();
        G[1].DataSource.ClearCategory("cv");
        G[1].DataSource.ClearCategory("load");
        G[1].DataSource.EndBatch();


    }

    // Update is called once per frame
    void Update()
    {
        float time = Time.time;
        if (lastTime + ts * 2 < time)
        {

            if (Setting.setting._selector.Equals("MANUAL"))
            {

                G[0].DataSource.AddPointToCategoryRealtime("pv", Time.time, (Setting.setting._pv), 0.5f);
                G[0].DataSource.AddPointToCategoryRealtime("sp", Time.time, (Setting.setting._cv) * 100f, 0.5f);
                G[1].DataSource.AddPointToCategoryRealtime("cv", Time.time, (Setting.setting._cv * 100f), 0.5f);
                G[1].DataSource.AddPointToCategoryRealtime("load", Time.time, (Setting.setting._di * 100f), 0.5f);
            }
            else
            {


                G[0].DataSource.AddPointToCategoryRealtime("pv", Time.time, (Setting.setting._pv), 0.5f);
                G[0].DataSource.AddPointToCategoryRealtime("sp", Time.time, (Setting.setting._sp), 0.5f);
                if (Setting.setting._control.Equals("FUZZY")) { G[1].DataSource.AddPointToCategoryRealtime("cv", Time.time, (Setting.setting._cv * 40f), 0.5f); }
                else { G[1].DataSource.AddPointToCategoryRealtime("cv", Time.time, (Setting.setting._cv * 100f), 0.5f); }

                G[1].DataSource.AddPointToCategoryRealtime("load", Time.time, (Setting.setting._di * 100f), 0.5f);
            }







        }
    }
}
