using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlPanel : MonoBehaviour
{
    private string Activador;
    [SerializeField] private GameObject[] Valve;

    private void OnTriggerStay(Collider other)
    {
        Activador = gameObject.name;

        if (Activador.Equals("Activadorvin")) { Valve[0].SetActive(true); }
        if (Activador.Equals("Activadorvout")) { Valve[1].SetActive(true); }

        this.Panel(other.tag);

    }

    private void OnTriggerExit(Collider other)
    {
        Activador = gameObject.name;

        if (Activador.Equals("Activadorvin")) { Valve[0].SetActive(false); }
        if (Activador.Equals("Activadorvout")) { Valve[1].SetActive(false); }


    }


    private void Panel(string tag)
    {




        if (Input.GetKey(KeyCode.F1)) { Setting.setting._selector = 1; this.Control("MANUAL"); }
        if (Input.GetKey(KeyCode.F2)) { Setting.setting._selector = 2; this.Control("PID"); }
        if (Input.GetKey(KeyCode.F3)) { Setting.setting._selector = 3; this.Control("MPC"); }
        if (Input.GetKey(KeyCode.F4)) { Setting.setting._selector = 4; this.Control("FUZZY"); }
        // if (Input.GetKey(KeyCode.F5)) { Setting.setting._selector = 5; }
        // if (Input.GetKey(KeyCode.F6)) { Setting.setting._selector = 6; }
        // if (Input.GetKey(KeyCode.F7)) { Setting.setting._selector = 7; }
        if (Input.GetKey(KeyCode.M)) { Setting.setting._cv = 0.7f; }
        if (Setting.setting._selector.Equals(1)) { this.MANUAL(); }
        if (Setting.setting._selector.Equals(2)) { this.PID(); }
        if (Setting.setting._selector.Equals(3)) { this.MPC(); }
        if (Setting.setting._selector.Equals(4)) { this.FUZZY(); }




    }

    private void MANUAL()
    {
        if (Input.GetKey(KeyCode.R)) { if (Setting.setting._cv >= 1f) { Setting.setting._cv = 1f; } else { Setting.setting._cv += 0.001f; } }
        else if (Input.GetKey(KeyCode.F)) { if (Setting.setting._cv <= 0f) { Setting.setting._cv = 0f; } else { Setting.setting._cv -= 0.001f; } }


        if (Input.GetKey(KeyCode.T)) { if (Setting.setting._di >= 1f) { Setting.setting._di = 1f; Setting.setting._v2a = 3600f; } else { Setting.setting._di += 0.001f; Setting.setting._v2a += 10f; } }
        else if (Input.GetKey(KeyCode.G)) { if (Setting.setting._di <= 0f) { Setting.setting._di = 0f; ; Setting.setting._v2a = 0f; } else { Setting.setting._di -= 0.001f; Setting.setting._v2a -= 10f; } }

        if (Input.GetKey(KeyCode.Y)) { Setting.setting._cv = 1f; } else if (Input.GetKey(KeyCode.H)) { Setting.setting._cv = 0f; }
        if (Input.GetKey(KeyCode.U)) { Setting.setting._di = 1f; } else if (Input.GetKey(KeyCode.J)) { Setting.setting._di = 0f; }


    }

    private void PID()
    {
        if (Input.GetKey(KeyCode.R)) { if (Setting.setting._sp >= 80f) { Setting.setting._sp = 80f; } else { Setting.setting._sp += 0.1f; } }
        else if (Input.GetKey(KeyCode.F)) { if (Setting.setting._sp <= 0f) { Setting.setting._sp = 0f; } else { Setting.setting._sp -= 0.1f; } }

        if (Input.GetKey(KeyCode.T)) { if (Setting.setting._di >= 1f) { Setting.setting._di = 1f; Setting.setting._v2a = 3600f; } else { Setting.setting._di += 0.01f; Setting.setting._v2a += 10f; } }
        else if (Input.GetKey(KeyCode.G)) { if (Setting.setting._di <= 0f) { Setting.setting._cv = 0f; ; Setting.setting._v2a = 0f; } else { Setting.setting._di -= 0.01f; Setting.setting._v2a -= 10f; } }
    }

    private void MPC()
    {
        if (Input.GetKey(KeyCode.R)) { if (Setting.setting._sp >= 80f) { Setting.setting._sp = 80f; } else { Setting.setting._sp += 0.1f; } }
        else if (Input.GetKey(KeyCode.F)) { if (Setting.setting._sp <= 0f) { Setting.setting._sp = 0f; } else { Setting.setting._sp -= 0.1f; } }

        if (Input.GetKey(KeyCode.T)) { if (Setting.setting._di >= 1f) { Setting.setting._di = 1f; Setting.setting._v2a = 3600f; } else { Setting.setting._di += 0.01f; Setting.setting._v2a += 10f; } }
        else if (Input.GetKey(KeyCode.G)) { if (Setting.setting._di <= 0f) { Setting.setting._di = 0f; ; Setting.setting._v2a = 0f; } else { Setting.setting._di -= 0.01f; Setting.setting._v2a -= 10f; } }
    }


    private void FUZZY()
    {
        if (Input.GetKey(KeyCode.R)) { if (Setting.setting._sp >= 80f) { Setting.setting._sp = 80f; } else { Setting.setting._sp += 0.1f; } }
        else if (Input.GetKey(KeyCode.F)) { if (Setting.setting._sp <= 0f) { Setting.setting._sp = 0f; } else { Setting.setting._sp -= 0.1f; } }

        if (Input.GetKey(KeyCode.T)) { if (Setting.setting._di >= 1f) { Setting.setting._di = 1f; Setting.setting._v2a = 3600f; } else { Setting.setting._di += 0.01f; Setting.setting._v2a += 10f; } }
        else if (Input.GetKey(KeyCode.G)) { if (Setting.setting._di <= 0f) { Setting.setting._di = 0f; ; Setting.setting._v2a = 0f; } else { Setting.setting._di -= 0.01f; Setting.setting._v2a -= 10f; } }
    }

    private void Control(string name)
    {
        Setting.setting._cv = 0;
        Setting.setting._control = name;
    }

}
