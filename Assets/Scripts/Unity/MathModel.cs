using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MathModel : MonoBehaviour
{
    private float ts = 0.1f;
    private float To;
    private float time;
    private float retardo;
    private float cv;
    private Memoryshare memoryshare;

    void Start()
    {
        #region MEMORIAS COMPARTIDAS
        memoryshare = new Memoryshare();
        memoryshare.freeMemoriShare();

        memoryshare.createMemoriShare("UNITY", 6, 2);
        memoryshare.createMemoriShare("MATLAB", 3, 2);
        memoryshare.createMemoriShare("ENABLE", 2, 1);

        memoryshare.openMemoryShare("UNITY", 2);
        memoryshare.openMemoryShare("MATLAB", 2);
        memoryshare.openMemoryShare("ENABLE", 1);
        #endregion


        for (int i = 0; i < 6; i++)
        {
            memoryshare.Setfloat("UNITY", 0f, i);
            memoryshare.Setfloat("MATLAB", 0f, i);
        }

        memoryshare.SetInt("ENABLE", 1, 0);
        memoryshare.SetInt("ENABLE", 0, 1);


    }

    void Update()
    {

        To += Time.deltaTime;

        if (To >= ts)
        {
            time += Time.deltaTime;
            memoryshare.SetInt("ENABLE", Setting.setting._selector, 1);

            if (Setting.setting._control.Equals("MANUAL")) { this.ControlManual(); }

            if (Setting.setting._control.Equals("PID")) { this.PID(); }

            if (Setting.setting._control.Equals("MPC")) { this.MPC(); }

            if (Setting.setting._control.Equals("FUZZY")) { this.FUZZY(); }

            To = 0f;
        }
    }

    // FUNCION DEL CONTROL MANUAL
    private void ControlManual()
    {
        memoryshare.Setfloat("UNITY", Setting.setting._pv, 0);
        memoryshare.Setfloat("UNITY", Setting.setting._cv, 1);

        this.ProcesoMatematico();
    }

    // FUNCION DEL CONTROLADOR PID
    private void PID()
    {
        Setting.setting._he = Setting.setting._sp - Setting.setting._pv;

        memoryshare.Setfloat("UNITY", Setting.setting._pv, 0);
        memoryshare.Setfloat("UNITY", Setting.setting._sp, 1);
        memoryshare.Setfloat("UNITY", Setting.setting._di, 2);
        memoryshare.Setfloat("UNITY", Setting.setting._kp, 3);
        memoryshare.Setfloat("UNITY", Setting.setting._Ti, 4);
        memoryshare.Setfloat("UNITY", Setting.setting._Td, 5);

        Setting.setting._cv = memoryshare.Getfloat("MATLAB", 0) / 100.0f;

        this.ProcesoMatematico();

    }

    // FUNCION DEL CONTROLADOR MPC
    private void MPC()
    {
        Setting.setting._he = Setting.setting._sp - Setting.setting._pv;

        memoryshare.Setfloat("UNITY", Setting.setting._pv, 0);
        memoryshare.Setfloat("UNITY", Setting.setting._sp, 1);
        memoryshare.Setfloat("UNITY", Setting.setting._di, 2);
        memoryshare.Setfloat("UNITY", Setting.setting._Na, 3);
        memoryshare.Setfloat("UNITY", Setting.setting._Nu, 4);
        memoryshare.Setfloat("UNITY", Setting.setting._alpha, 5);
        memoryshare.Setfloat("UNITY", Setting.setting._beta, 6);

        Setting.setting._cv = memoryshare.Getfloat("MATLAB", 0) / 100.0f;
        this.ProcesoMatematico();
    }

    // FUNCION DEL CONTROLADOR DIFUSO
    private void FUZZY()
    {
        Setting.setting._he = Setting.setting._sp - Setting.setting._pv;

        memoryshare.Setfloat("UNITY", Setting.setting._pv, 0);
        memoryshare.Setfloat("UNITY", Setting.setting._sp, 1);
        memoryshare.Setfloat("UNITY", Setting.setting._di, 2);

        Setting.setting._cv = memoryshare.Getfloat("MATLAB", 0) / 40.0f;
        this.ProcesoMatematico();
    }

    private void ProcesoMatematico()
    {

        // PARAMETROS DEL TANQUE
        float C = 0.53821693f;
        float R = 1.39999718f;
        // CONSTANTES DE LA VALVULAS
        float k1 = 0.08684219f;
        float k2 = 0.07908202f;


        float pp, p, a2;
        float pi, po;

        p = Setting.setting._pv;
        pi = Setting.setting._pi;
        po = Setting.setting._po;

        a2 = Setting.setting._di;

        retardo += Time.deltaTime;
        if (retardo >= R) { cv = Setting.setting._cv; retardo = 0f; }

        // PROCES MODEL
        pp = C * (cv * k1 * Mathf.Sqrt(pi * (pi - p)) - (a2 * k2 * Mathf.Sqrt(p * (p - po))));

        // EULER
        p = p + pp * ts;

        // ASEGURAR QUE NO SE HAGA IMAGINARIO
        p = Mathf.Min(80.0f, Mathf.Max(0.0f, p));

        // ACTUALIZAR EL VALOR DE EL PV
        Setting.setting._pv = p;

        if (Setting.setting._pv >= 80f) { Setting.setting._dmaximo = true; } else { Setting.setting._dmaximo = false; }


    }



    void OnApplicationQuit()
    {
        memoryshare.SetInt("ENABLE", 0, 0);
        memoryshare.freeMemoriShare();
    }
}
