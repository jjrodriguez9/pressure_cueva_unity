using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Instrumentos : MonoBehaviour
{
    public GameObject[] valve;
    public GameObject[] gage;
    private Vector3 deslizar, rotar;
    void Start()
    {
        valve[0].transform.localPosition = new Vector3(0f, Setting.setting._cv * -0.0687f, 0f);

        deslizar = valve[1].transform.localPosition;
        deslizar.y = 0.673f + (0.099f * Setting.setting._di);

        rotar = valve[1].transform.localEulerAngles;

    }


    void Update()
    {
        valve[0].transform.localPosition = new Vector3(0f, Setting.setting._cv * 0.0605f, 0f);

        deslizar.y = 0.673f + (0.099f * Setting.setting._di);
        valve[1].transform.localPosition = deslizar;

        rotar.y = Setting.setting._v2a;
        valve[1].transform.localEulerAngles = rotar;

        if (Setting.setting._pv < 20f) { gage[0].transform.localEulerAngles = new Vector3(0f, -Setting.setting._pv, 0f); }
        if (Setting.setting._pv > 20f && Setting.setting._pv < 40f) { gage[0].transform.localEulerAngles = new Vector3(0f, -Setting.setting._pv - 5f, 0f); }
        if (Setting.setting._pv > 40f && Setting.setting._pv < 60f) { gage[0].transform.localEulerAngles = new Vector3(0f, -Setting.setting._pv - 11f, 0f); }
        if (Setting.setting._pv > 60f && Setting.setting._pv < 80f) { gage[0].transform.localEulerAngles = new Vector3(0f, -Setting.setting._pv - 21f, 0f); }
        if (Setting.setting._pv > 80f && Setting.setting._pv < 100f) { gage[0].transform.localEulerAngles = new Vector3(0f, -Setting.setting._pv - 28f, 0f); }


        if (Setting.setting._pi < 20f) { gage[1].transform.localEulerAngles = new Vector3(0f, -Setting.setting._pi, 0f); }
        if (Setting.setting._pi > 20f && Setting.setting._pi < 40f) { gage[1].transform.localEulerAngles = new Vector3(0f, -Setting.setting._pi - 5f, 0f); }
        if (Setting.setting._pi > 40f && Setting.setting._pi < 60f) { gage[1].transform.localEulerAngles = new Vector3(0f, -Setting.setting._pi - 11f, 0f); }
        if (Setting.setting._pi > 60f && Setting.setting._pi < 80f) { gage[1].transform.localEulerAngles = new Vector3(0f, -Setting.setting._pi - 21f, 0f); }
        if (Setting.setting._pi > 80f && Setting.setting._pi <= 100f) { gage[1].transform.localEulerAngles = new Vector3(0f, -Setting.setting._pi - 28f, 0f); }

    }

}

